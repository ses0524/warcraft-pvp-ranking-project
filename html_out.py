import pvp

data = pvp.Get_Top_Ten(pvp.leaders)

def html_write():

    rows = ""
    for item in data:
        columns = ""
        for i in item:
            columns += "<td>{}</td>".format(i)
        rows += '<tr>{}</tr>'.format(columns)
    
    html = """
    <html>
        <head>
            <link rel="stylesheet" href="/static/css/style.css">
            <title>Data from my API</title>
        </head>
        <body>
        Current Retail WoW 3v3 PVP rankings:
        <table>
            <tr>
                <th>Rank</th>
                <th>Rating</th>
                <th>Character</th>
                <th>Class</th>
                <th>Faction</th>
            </tr>
            {}
        </table>
        </body>
    </html>
    """.format(rows)

    return html