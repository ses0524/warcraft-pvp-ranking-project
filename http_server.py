import os, os.path
import cherrypy
import html_out



class ServeData(object):
    @cherrypy.expose
    def index(self):
        return html_out.html_write()

if __name__ == "__main__":
    conf = {
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': os.path.abspath(os.getcwd())
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './public'
        }
    }
    cherrypy.quickstart(ServeData(), '/', conf)
