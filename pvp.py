import requests
from requests.auth import HTTPBasicAuth
import json
from config import api_access_token, api_access_token_logs

leader_board_file = open("entries.json","w")


def Get_Leader_Board():
    leaderboard_url = "https://us.api.blizzard.com/data/wow/pvp-season/34/pvp-leaderboard/3v3?namespace=dynamic-us&locale=en_US&access_token="
    leaderboard_url_token = leaderboard_url + api_access_token
    getURL = requests.get(leaderboard_url_token)

    leaderBoard = getURL.json()
    return leaderBoard

def Get_Character_Info(realmSlug, characterName):
    character_profile_url = "https://us.api.blizzard.com/profile/wow/character/{0}/{1}?namespace=profile-us&locale=en_US&access_token=".format(realmSlug,characterName)
    character_profile_url_token = character_profile_url + api_access_token
    getURL = requests.get(character_profile_url_token)

    characterInfo = getURL.json()
    return characterInfo


leaders = Get_Leader_Board()
leader_board_file.write(json.dumps(leaders))
leader_board_file.close()


def Get_Top_Ten(leadersJson):
    entriesCount = 0
    top_players_array = [[]]

    for item in leadersJson['entries']:
        while entriesCount != 10:
            character = Get_Character_Info(leadersJson['entries'][entriesCount]['character']['realm']['slug'],leadersJson['entries'][entriesCount]['character']['name'].lower())
            arr = []
            arr.append(leadersJson['entries'][entriesCount]['rank'])
            arr.append(leadersJson['entries'][entriesCount]['rating'])
            arr.append(leadersJson['entries'][entriesCount]['character']['name'])
            arr.append(character["character_class"]['name'])
            arr.append(leadersJson['entries'][entriesCount]['faction']['type'])

            top_players_array.append(arr)
            
            entriesCount += 1
    
    return top_players_array

character = Get_Character_Info(leaders['entries'][0]['character']['realm']['slug'],leaders['entries'][0]['character']['name'].lower())

top_player_name = leaders['entries'][0]['character']['name']
top_player_class = character["character_class"]['name']
top_playerFaction = leaders['entries'][0]['faction']['type']
